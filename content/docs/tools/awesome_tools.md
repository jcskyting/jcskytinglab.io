---
title: "Awesome Tools"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Storage

## ceph
分散式儲存系統

## GlusterFS / glusterd

# monit

## Prometheus

## collectd

## Datadog

# VPN

## IPsec

- [Github repo: Scripts to build your own IPsec VPN server, with IPsec/L2TP and Cisco IPsec on Ubuntu, Debian and CentOS](https://github.com/hwdsl2/setup-ipsec-vpn)

# Data visulization

## commercial
### Tableau
### Looker

## Open Source
### Superset: Airbnb, Apache Incubator
### Redash
### Metabase

# JS draw

## D3
## [ECharts](https://echarts.apache.org/examples/en/index.html#chart-type-graphGL)
