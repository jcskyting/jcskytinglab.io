---
title: "K8s Kubeadm Ubuntu"
weight: 1
draft: true
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

kubeadm commnads
```
kubeadm init
kubeadm reset
kubeadm init --ignore-preflight-errors=all
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=0.0.0.0
``````

[Official Doc](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

kubeadm preflight error shooting
[ERROR IsPrivilegedUser]: user is not running as root
`sudo kubeadm init`

[WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
error execution phase preflight: [preflight] Some fatal errors occurred
[fix way](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)

[ERROR Swap]: running with swap on is not supported. Please disable swap


[CNI list](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#pod-network)
[CNI Compare - zh-TW](https://kknews.cc/zh-tw/code/kv89ojb.html)
weaver: `kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"`

## disable swap
`sudo swapoff -a`
`sudo vi /etc/fstab` and disable swap files

## issues
validation.go:28] Cannot validate kubelet config - no validator is available
validation.go:28] Cannot validate kube-proxy config - no validator is available

## issue 2
kubectl apply but error, unable to connect to the server
ibdopt@ibdopt-44:~$ kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
Unable to connect to the server: dial tcp: lookup cloud.weave.works on 127.0.0.53:53: read udp 127.0.0.1:51919->127.0.0.53:53: i/o timeout
[ref post](https://www.simpleapples.com/2018/07/15/solving-kubernetes-dns-problem/)

- issue 3
```
$ sudo kubelet --resolv-conf /run/systemd/resolve/resolv.conf
> failed to run Kubelet: failed to create kubelet: misconfiguration: kubelet cgroup driver: "cgroupfs" is different from docker cgroup driver: "systemd"
```

## install add-on

- [Kubernetes Dashboard](https://github.com/kubernetes/dashboard)
if you want open dashboard on remote use `kubectl proxy --address='0.0.0.0' --accept-hosts='^*$' --accept-paths='^.*'`
get login token `kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')`
Accessing the Dashboard UI: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/.

## autocomplete
```bash
source <(kubectl completion bash) # setup autocomplete in bash into the current shell, bash-completion package should be installed first.
echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to your bash shell.
```