---
title: "Kubernetes"
bookFlatSection: true
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Kubernetes usage and notes

[Basic knowledge ref(zh-TW)](https://medium.com/@C.W.Hu/kubernetes-basic-concept-tutorial-e033e3504ec0)

kubelet: primary “node agent” that runs on each node

kubectl: command line tools

# production environment k8s deployment tools

kubeadm: bootstrap a minimum viable Kubernetes cluster that conforms to best practices.
limitations:
  - Cluster resilience: has a single control-plane node, with a single etcd database running on it.
  - [Set up a High Availability etcd cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/)
  - [Ref: use kubeadm build k8s cluster on ubuntu @2018-05-14](https://blog.yowko.com/ubuntu-kubeadm-kubernetes/)

[how to choose between kubeadm, kops, KRIB, kubespray - yiidtw@2018-05-15](https://yiidtw.github.io/blog/2018-05-15-install-kubernetes-1-9-with-HA-using-kubeadm/)