---
title: "kubernetes Ecosystem"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

[helmfile](https://github.com/roboll/helmfile)

## cluster
[vishwakarma: create aws k8s cluster with spot instance to save money](https://github.com/getamis/vishwakarma)

## Prometheus
Ref: https://medium.com/starbugs/%E7%85%A7%E9%A1%A7-prometheus-%E7%9A%84%E7%94%9F%E6%B4%BB%E8%B5%B7%E5%B1%85-e2d7a8997b86

[coreos/prometheus-operator](https://github.com/coreos/prometheus-operator)

[coreos/kube-prometheus](https://github.com/coreos/kube-prometheus)

## kafka

### helm open source charts

[helm](https://github.com/helm/charts/tree/master/incubator/kafka)

[Yolean - personal but papular](https://github.com/Yolean/kubernetes-kafka)

### commercial provider

#### confluent
  - confluent operator
> limitation: free forever with 1 kafka broker

#### banzaicloud
  - [banzaicloud operator](https://github.com/banzaicloud/kafka-operator)

#### [Archetech](https://github.com/banzaicloud/kafka-operator/blob/master/docs/img/kafka-operator-arch.png)
> limitation: need upgrade to SUPERTUBES PRO version has zookeeper

#### [Bitnami](https://bitnami.com/stack/kafka/helm)


