---
title: "K8s configmap and secret usage"
weight: 1
draft: true
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

## secret

input secret from kubecli
```shell
$ kubectl create secret generic demo-secret-from-literal --from-literal=username=root
```