---
title: "K8s commands"
weight: 1
draft: true
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

```shell
kubectl config view

kubectl get nodes

kubectl describe node ${nodename}

kubectl get pods -n kube-system
kubectl get pods --all-namespaces
kubectl get pods,services,deployment,cronjobs,configmaps,replicaset
kubectl get po,services,deploy,cronjobs,cm,rs
kubectl cluster-info
kubectl get pods -o wide --show-labels=true
kubectl diff -R -f configs/
kubectl apply -R -f configs/
kubectl get pods/<podname> -o yaml # check pods setting with yaml
```