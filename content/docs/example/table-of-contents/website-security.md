---
title: Website Security
weight: 1
---
# Scan tools

## Nmap

A tools for scan PORT, STATE, SERVICE, OS and others.

### usage example

- basic report(PORT, STATE, SERVICE, OS)
`nmap -A scanme.nmap.org`

- scan host OS 
`nmap -O scanme.nmap.org`
> Aggressive OS guesses: Linux 2.6.32 - 3.9 (98%), Linux 2.6.38 - 3.0 (97%), Linux 2.6.32 - 2.6.39 (97%), Netgear DG834G WAP or Western Digital WD TV media player (96%), Linux 2.6.32 - 3.2 (95%), Linux 3.0 - 3.9 (95%), Linux 3.2 (95%), Linux 2.6.32 - 3.6 (95%), Linux 3.1 (95%), AXIS 210A or 211 Network Camera (Linux 2.6) (94%)
No exact OS matches for host (test conditions non-ideal).

- ping scan
`nmap -sP 140.115.35.0/24`

## password scan
[hydra, ncraft, medusa](https://hackertarget.com/brute-forcing-passwords-with-ncrack-hydra-and-medusa/)