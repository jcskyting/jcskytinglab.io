---
title: "proxy setting"
date: 2020-04-17T08:38:19+08:00
draft: true
---

Reference this article

https://phoenixnap.com/kb/setup-install-squid-proxy-server-ubuntu

```
sudo apt-get update
sudo apt-get install squid

```

```
sudo nano /etc/squid/squid.conf
```

add `http_access allow all`
delete `http_access deny all`

```
sudo systemctl restart squid
```

---

```
10.60.10.130
ibdo
ibod2018
```
---

####docker use proxy:
1.Edit /etc/default/docker

```
sudo nano /etc/default/docker
```
2.Add following line

```
export http_proxy="http://10.60.10.128:3128/"
```
3.Restart docker

```
service docker restart
```

---

####docker-compose use proxy:
1.Create folder for configuring docker service through systemd

```
mkdir /etc/systemd/system/docker.service.d
```
2.Create service configuration file at `/etc/systemd/system/docker.service.d/http-proxy.conf` and put the following in the newly created file

```
[Service]
 # NO_PROXY is optional and can be removed if not needed
 # Change proxy_url to your proxy IP or FQDN and proxy_port to your proxy port
 Environment="HTTP_PROXY=http://10.60.10.128:3128" "NO_PROXY=localhost,127.0.0.0/8"
```
3.Restart docker

```
sudo systemctl daemon-reload
sudo systemctl show docker --property Environment
sudo systemctl restart docker
```

---

#### wget
add the following lines to `~/.wgetrc` to use proxy

```
use_proxy=yes
http_proxy=10.60.10.128:3128
https_proxy=10.60.10.128:3128
```

---

#### docker build
add the following json into `~/.docker/config.json`.

```
{                                            
 "proxies":                                  
 {                                           
   "default":                                
   {                                         
     "httpProxy": "http://10.60.10.128:3128",
     "httpsProxy": "http://10.60.10.128:3128"
   }                                         
 }                                           
}                                            
```

---

### apt 使用proxy需另外設定
add the following lines to /etc/apt/apt.conf to use proxy

```
Acquire::http::Proxy "http://10.60.10.128:3128";
```

---

###  “Release file is not yet valid”

#### Set date

```
sudo date -s "Thu Aug  9 21:31:26 UTC 2012"
```