---
title: "Nuxt With Express"
date: 2020-04-29T15:09:47+08:00
---

```bash
# Create project
yarn create nuxt-app project-name
...
🎉  Successfully created project stock-big-data

  To get started:

	cd stock-big-data
	yarn dev

  To build & start for production:

	cd stock-big-data
	yarn build
	yarn start

  To test:

	cd stock-big-data
	yarn test
```