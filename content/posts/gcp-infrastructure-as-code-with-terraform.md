---
title: "GCP Infrastructure as code with Terraform"
description: ""
tags:
  - "Terraform"
  - "GCP"
date: 2020-03-05T17:19:59+08:00
categories:
  - "Software"
---


# GCP Infrastructure as code with Terraform

## Why Infrastructure as code

- reduce cost
    - What will happen in a big team
- easy maintain & deploy
    - docker, ansible, puppet, chef ...
    - How about GCP, AWS, Azure??
    - how about multi environment, ex: dev, staging, production
- keep knowledge
    - You are a awesome leader~!

## What choice do we have on IaC

- AWS: CDK(TS, python, ..), cloudformation(json/yaml)
- GCP: Deployment Manager(python, yaml, schema)
- Azure: Deployment Manager(json)
- Multicloud: Terraform or Handmade

## Terraform introduction

Made by **HashiCorp**

## Why Terraform

- Multicloud
- stackshare
![](https://i.imgur.com/3Q0yP4A.png)
![](https://i.imgur.com/XXQUtuh.png)
![](https://i.imgur.com/HD2ZKqN.png)

# Terraform usage sharing

[TOC]

## Installation

Check out on official site by yourself!

## Common cli usage

1. init terraform
`terrafrom init`
must run in first time
generate .terraform cache folder
download plugins according to your tf script
![](https://i.imgur.com/NFdOrzB.png)


2. create/update infra
`terrafrom apply`
create and update infra according to your tf script
It will include all of `*.tf` files in working directory
genrate `.tfstate` file. **keep the state file forever**
> Terraform must store state about your managed infrastructure and configuration.
![](https://i.imgur.com/53dyWDJ.png)

3. destory infra
`terrafrom destory`
destroy infra accroding to `.tfstate` state file

## How about the code

following sample code can be append in to a `play.tf` file

### prepare steps

1. remote state config with gcs

Terraform remote state: We need to store `.tfstate` state file in gcs for a team/project use.

```
terraform {
  backend "gcs" {
    bucket = "your-gcs-bucket-name"
    prefix = "prod"
  }
}
```
2. integrate gcp

export `GOOGLE_APPLICATION_CREDENTIALS` environment var for gcp auth.
```
provider "google" {
  project = "your-project-name"
  region  = "asia-east2"
}
```
### create resource

3. cloud scheduler

```
resource "google_cloud_scheduler_job" "job" {
  name      = "your-scheduler-name"
  schedule  = "0 15 * * *"
  time_zone = "Asia/Taipei"
  http_target {
    http_method = "POST"
    uri         = "https://google.com"
    oauth_token {
      service_account_email = "account@***.iam.gserviceaccount.com"
    }
  }
}
```
[more example](https://www.terraform.io/docs/providers/google/r/cloud_scheduler_job.html)

4. bigquery

dataset [document](https://www.terraform.io/docs/providers/google/r/bigquery_dataset.html)
```
resource "google_bigquery_dataset" "dataset" {
  dataset_id                  = "example_dataset"
  friendly_name               = "test"
  description                 = "This is a test description"
  location                    = "EU"
  default_table_expiration_ms = 3600000
}

```
table [document]()
```
resource "google_bigquery_table" "default" {
  dataset_id = google_bigquery_dataset.default.dataset_id
  table_id   = "bar"

  time_partitioning {
    type = "DAY"
    field = "your-partition-field"
  }
  schema = file("${path.module}/table_schema.json")
}
```
```json
// table_schema.json
[
  {
    "name": "permalink",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "The Permalink"
  },
  {
    "name": "state",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "State where the head office is located"
  }
]
```

### other functions

1. local variables
```
locals {
  location = "asia-east1"
}

resource "google_bigquery_dataset" "ibdo" {
  dataset_id    = "tf_open_data"
  friendly_name = "tf_open_data"
  description   = "open data project dataset build with terraform"
  location      = "asia-east1"

  labels = {
    maintainer = local.location
  }
}
```

2. [functions](https://www.terraform.io/docs/configuration/functions/abs.html)

## Conclusion

Terraform vs AWS Cloudformation(yaml/json)
Pros: easy to write/maintain
Cons: version issue, native tool like aws cloudformation/CDK will support the more later service version than 3rd lib like terraform.