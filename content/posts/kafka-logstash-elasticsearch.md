---
title: "Kafka Logstash Elasticsearch"
date: 2020-05-05T09:39:37+08:00
---

helm chart
https://github.com/helm/charts/tree/master/stable/logstash

```json
// normal data: `{"test_key": "test_value_4"}`
{
    "_index": "sky-logstash-test0",
    "_type": "_doc",
    "_id": "IQp24nEB2wvqg1dVShNg",
    "_score": 1.0,
    "_source": {
        "@version": "1",
        "test_key": "test_value_4",
        "@timestamp": "2020-05-05T01:32:15.256Z",
        "type": "example"
    }
},
// json parse error data: `string`
// The original message will be save to message key
{
    "_index": "sky-logstash-test0",
    "_type": "_doc",
    "_id": "FAp44nEB2wvqg1dVURtq",
    "_score": 1.0,
    "_source": {
        "@version": "1",
        "message": "string",
        "tags": [
            "_jsonparsefailure"
        ],
        "@timestamp": "2020-05-05T01:34:28.142Z",
        "type": "example"
    }
}
```
[logstash codec plugins](https://www.elastic.co/guide/en/logstash/7.6/codec-plugins.html)

[logstash filter grok patterns: predefine regex](https://github.com/logstash-plugins/logstash-patterns-core/blob/master/patterns/grok-patterns)
