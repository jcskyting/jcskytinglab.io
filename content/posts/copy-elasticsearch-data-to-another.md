---
title: "Copy Elasticsearch Data to Another"
date: 2020-05-12T08:18:22+08:00
---

```shell
source_es="http://${es-host}:9200"
destination_es="http://${es-dest}:9200"
index_url="${source_es}/_cat/indices/index-pattern*?format=json"
reindex_url="${destination_es}/_reindex"

generate_post_data()
{
  cat <<EOF
{
  "source": {
    "remote": {
      "host": ${source_es}"
    },
    "index": "${index}",
    "query": {
      "match_all": {}
    }
  },
  "dest": {
    "index": "${index}",
    "type": "mappings"
  }
}
EOF
}


curl -s ${index_url} | jq -r '.[].index' | while read index ; do
    echo "index: ${index}"
    curl -X POST \
      ${reindex_url} \
      -H 'Content-Type: application/json' \
      --data "$(generate_post_data)"
done
```