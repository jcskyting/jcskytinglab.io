---
title: "Python Tip"
date: 2020-03-19T10:08:29+08:00
Tags:
  - python
Categories:
  - Software
---

# python's _ in variable
[python's _ in variable](https://aji.tw/python%E4%BD%A0%E5%88%B0%E5%BA%95%E6%98%AF%E5%9C%A8__%E5%BA%95%E7%B7%9A__%E4%BB%80%E9%BA%BC%E5%95%A6/)

# method params with ** and *

```python
import json
def demo(url, params):
    print(url)
    print(params)

dict_data = {"url": "url", "params": {"json": True}}
demo(**dict_data)
# =====
# url
# {'json': True}
# =====

list_data = ["url", {"json": True}]
demo(*list_data)
# =====
# url
# {'json': True}
# =====
```

# contextmanager

## file operation
```python
from contextlib import contextmanager

# Define Context Manager
@contextmanager
def open_file(name, mode):
  f = open(name, mode)
  yield f
  f.close()

with open_file('file.txt', 'w') as f:
  f.write("Hello, world.")
```

## Edit env variable
```python
@contextmanager
def environ(env):
    original_environ = os.environ.copy()
    os.environ.update(env)
    yield
    os.environ = original_environ

proxy_environ = { 'http_proxy': 'http_proxy' }
with environ(proxy_environ):
    print(f'{os.getenv("http_proxy")}')
# =====
# http_proxy
# =====
```