---
title: "Linux Command Notes"
date: 2020-03-11T15:06:32+08:00
tags:
  - "Linux"
  - "GCP"
categories:
  - "Software"
---

```bash
# Ubuntu
> hostnamectl status
   Static hostname: ibdopt-44
         Icon name: computer-desktop
           Chassis: desktop
        Machine ID: 09241a84d9834cc8aca98b7743e9f48f
           Boot ID: b5bd9ea52c34427c9c05c6818bd054d6
  Operating System: Ubuntu 18.04.2 LTS
            Kernel: Linux 5.3.0-28-generic
      Architecture: x86-64
```

check port
```bash
netstat -tulpn | grep :80
lsof -i -P -n | grep :80
```

watch command result per 1 second
`watch -d -n 1 commad`

Find files modified within 60 minutes
`find /tmp -type f -mmin -60`

Is file modified within 60 minutes? If not, exit with 1.
test -n "$(find a.txt -type f -mmin -60)"


Redirect stdout to a file (>out), and then redirect stderr to stdout (2>&1):
`command >out 2>&1`

List how many disk and size
`lsblk`

OS info
`uname -a`

Disk performance monit (disk IO)
- Iostat
- Iozone
- SAR
- vmstat

View CPU information
`cat /proc/cpuinfo`