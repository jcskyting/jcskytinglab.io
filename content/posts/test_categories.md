---
title: "Test_categories"
date: 2020-05-25T10:46:33+08:00
---

[Reference](https://circleci.com/blog/testing-methods-all-developers-should-know/?fbclid=IwAR04h-JLUDwWkJB7dtrqNl_sJScwXMfVsD1yCiMqNwUP9vuc61GlRiFCY7Y)

★  Unit testing: 測試邏輯的完整性，測出每一段程式碼應該要做的事情
★  Integration testing: 測試在不同元件或是模組間彼此關係和資料流程的完整性
★  End-to-End testing (E2E, System): 確保整個應用程式或是系統運作符合預期
★  Acceptance testing: 驗證產品跟功能是否有根據客戶所提出的規格來進行開發
★  White box testing (structural, clear box): 在軟體詳的細資訊和內部工作方式是已知的情況下來進行測試
★  Black box testing (functional, behavioral, closed box): 在軟體詳的細資訊和內部工作方式是未知的情況下來進行測試
★  Gray box testing: 因為黑箱跟白箱測試可能會導致某些重要的功能沒有測試到，所以有了灰箱測試，既可以輕鬆地實現黑箱測試 (輸入->輸出)，並且又可以針對特定的程式碼進行白箱測試
★  Manual testing: 手動測試當然就是使用者手動輸入東西並且跟系統進行互動，最後也是由使用者來評斷測試的結果
★  Static testing: 不真的去執行程式碼的測試方式，就像手動測試一樣會比較緩慢，但可以提早發現問題，例如有些公司會有 Code Review 的機制
★  Dynamic testing: 實際執行代碼的測試方式，其實上面提到的測試方式，大部分都算是在此類別中，除了手動和某些 acceptance testing
★  UI/Visual testing (browser testing): 針對使用者介面操作行為的測試方式
★ Smoke testing: 相對小的測試案例集合，用來驗證系統是否可以正常運作
★  Regression testing: 用來驗證先前可以正常使用的功能是否都還可以運行無誤
★  Load testing: 測試應用程式在工作量增加的情況下會如何回應
★  Penetration testing: 滲透測試為一種安全測試，用來驗證應用程式的安全穩定性