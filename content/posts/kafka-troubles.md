---
title: "Kafka Troubles"
date: 2020-03-31T13:57:14+08:00
---

Change kafka partition from 1 to 3, the custom python script end to send data to kafka-topic. Logs:
%5|1585559251.384|PARTCNT|rdkafka#producer-1| [thrd:main]: Topic iiot-mqtt-demo-topic partition count changed from 1 to 3
