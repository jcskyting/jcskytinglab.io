---
title: ""
date: 2020-05-04T09:25:16+08:00
---

## For maximum write performance, you should
[Reference](https://discuss.elastic.co/t/looking-for-heavy-write-optimization/17768/8)
use fastest disk subsystem (SSD)
use RAID 0 with expensive controller to max out IO bandwidth
do not run more than one ES instance per server
do not use virtual servers, use physical servers
for ES data folder, disable acess time flag (noatime), check filesystem
settings for more tweaking
use mlockall and mmapfs (niofs is not faster than mmapfs)
us bulk indexing (note that UDP is not reliable network protocol)
reserve enough off-heap memory for mmapfs (you do not need large heaps
for fast bulk indexing)
monitor network bandwidth for enough capacity
for high sustainable bulk write ES configuration, see
https://gist.github.com/jprante/10666960 39
on SSD, disable Linux CFQ scheduler for noop scheduler, see Drew Raines'
presentation https://speakerdeck.com/drewr/life-after-ec2 14


## check that FS type
ext2! That's ancient! Try ext3, ext4, or xfs. If you turn off
journaling things will be faster