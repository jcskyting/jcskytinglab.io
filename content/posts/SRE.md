---
title: "SRE"
date: 2020-04-28T17:31:37+08:00
---

Ref: https://medium.com/kkstream/%E5%A5%BD%E6%96%87%E7%BF%BB%E8%AD%AF-%E4%BD%A0%E5%9C%A8%E6%89%BE%E7%9A%84%E6%98%AF-sre-%E9%82%84%E6%98%AF-devops-2ded43c2852

Service Level Indicators(SLIs) 與 Service Level Objectives (SLOs)。
SLIs 定義了和系統「回應時間」相關的指標，例如回應時間，每秒的吞吐量，請求量，等等，常常會將這個指標轉化為比率或平均值。
SLOs 則是和相關人員討論後，得出的一個時間區間，期望 SLIs 所能維持一定水準的數字，例如「每個月 SLIs 要有如何的水準」，比較偏內部的指標。
Service Level Agreements (SLAs)，即使這不是 SRE 每天所關心的數字。作為一個線上服務的提供者，SLA 是對客戶的承諾，確保服務持續運行的百分比，通常是和客戶「談」出來的，每年(或每月)的停機時間不得低於幾分鐘。