---
title: "Js Destructuring Assignment on Function"
date: 2020-03-06T16:47:44+08:00
Tags:
  - "js"
Categories:
  - "Software"
menu: "main"
---

```js
function b(type = 'GET', 
{ data: data, success: success, error: error, contentType: contentType } = {}) {
  console.log('data: ' + data)
  console.log('success: ' + success)
  console.log('error: ' + error)
  console.log('contentType: ' + contentType)
}
b('p')
// data: undefined
// success: undefined
// error: undefined
// contentType: undefined
b('p', {data: 'this is data'})
// data: this is data
// success: undefined
// error: undefined
// contentType: undefined
b('p', {data: 'this is data', contentType: 'this is contentType'})
// data: this is data
// success: undefined
// error: undefined
// contentType: this is contentType
```

```js
function b(type = 'GET', { 
  data: data, 
  success: success, 
  error: error, 
  contentType: contentType 
  } = {}) {
  console.log('data: ' + data)
  console.log('success: ' + success)
  console.log('error: ' + error)
  console.log('contentType: ' + contentType)
}
```