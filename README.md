jcskyting.gitlab.io

# commands

- run server
`hugo server --minify --theme book`

- build
`hugo --minify --theme book`

- new post
`hugo new posts/my-first-post.md`